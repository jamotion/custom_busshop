from openerp.report import report_sxw
from openerp.report.report_sxw import rml_parse
from openerp.tools import mod10r
import logging

_logger = logging.getLogger(__name__)

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.context = context
        self.localcontext.update({
            'get_esr_codebar': self.get_esr_codebar,
            'getRappen': self.getRappen,
            'hasHourLines': self.hasHourLines,
            'getTotalHours': self.getTotalHours,
            'getUomHours': self.getUomHours,
        })

    def get_esr_codebar(self, inv):
        ref = mod10r('01' + str('%.2f' % inv.amount_total).replace('.', '').rjust(10, '0')) + '>'
        ref += str(inv.bvr_reference).replace(' ', '') + '+ '
        acc_num_arr = str(inv.partner_bank_id.acc_number).split('-')
        if len(acc_num_arr)==3:
            ref += acc_num_arr[0] + str(acc_num_arr[1]).rjust(6, '0') + acc_num_arr[2] + '>'
        else:
            ref += 'xxx-draft-xxx'

        return ref

    # Liefert die Rappen des Rechnungsbetrages als String mit Nullen aufgefuellt
    def getRappen(self, inv):
        rappen = int((inv.amount_total - int(inv.amount_total) + 0.005)*100)
        if rappen == 0:
            return '00'
        if rappen < 10:
            return '0' + str(rappen)
        return str(rappen)

    # Liefert True falls die Summe der verrechneten Stunden > 0 und mehr als eine Zeile mit Stunden vorhanden ist
    # TODO replace hard coded id 5
    def hasHourLines(self, inv):
        quantity = 0.0
        count = 0
        for line in inv.invoice_line:
            if line.uos_id.id == 5:
                quantity += line.quantity
                count += 1
        _logger.warn("hasHourLines. Quantity: %s, Count: %s " % (quantity,count))
        return (quantity > 0.0) and (count > 1)

    # Liefert die Summe der verrechneten Stunden
    # TODO replace hard coded id 5
    def getTotalHours(self, inv):
        quantity = 0.0
        for line in inv.invoice_line:
            if line.uos_id.id == 5:
                quantity += line.quantity
        return quantity

    # Liefert die konfigurierte Einheit der Stunden; z.B. Stunde(n)
    # TODO replace hard coded id 5
    def getUomHours(self, inv):
        for line in inv.invoice_line:
            if line.uos_id.id == 5:
                return line.uos_id.name
        return ''

