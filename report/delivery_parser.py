from openerp.report import report_sxw

import logging

_logger = logging.getLogger(__name__)

class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.context = context
        self.cr = cr
        self.uid = uid
        self.localcontext.update({
            'get_price': self.get_price,
            'get_lag': self.get_lag,
            'get_lag_lines': self.get_lag_lines,
        })

    def get_price(self, line):
        solineobj = self.pool.get('sale.order.line')
        solineid = solineobj.search(self.cr,self.uid,[('product_id', '=',line.product_id.id),('order_id','=',line.picking_id.sale_id.id)])
        if solineid:
            soline = solineobj.browse(self.cr,self.uid,solineid)[0]
            return soline.price_unit
        return float(0)

    def get_lag(self, line):
        moveobj = self.pool.get('stock.move')
        move_ids = moveobj.search(self.cr,self.uid,[('split_from','=',line.id)], limit=1)
        if move_ids:
            move = moveobj.read(self.cr,self.uid,move_ids,['product_qty'])
            return move[0]['product_qty']
        return float(0)

    def get_lag_lines(self, pick):
        pickobj = self.pool.get('stock.picking')
        moveobj = self.pool.get('stock.move')
        pick_ids = pickobj.search(self.cr,self.uid,[('backorder_id','=',pick.id)], limit=1)
        if pick_ids:
            pick_id = pick_ids[0]
            move_ids = moveobj.search(self.cr,self.uid,[('picking_id','=',pick_id),('split_from','=',None)], limit=1)
            return moveobj.browse(self.cr,self.uid,move_ids)
        return []

