from openerp.osv import fields,osv

class stock_picking(osv.osv):

    _inherit = "stock.picking"

    # gets the quali night code based on the delivery address
    def _get_quali_night_code(self, cr, uid, ids, arg, fields, context=None):
        res = {}

        for pick in self.browse(cr, uid, ids):
            res[pick.id] = pick.partner_id.quali_night_code

        return res

    _columns = {
        'quali_night_code':fields.function(_get_quali_night_code, "Quali night code", type="char"),
    }

stock_picking()
