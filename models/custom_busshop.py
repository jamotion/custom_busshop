import logging

from openerp import models, fields, api

_logger = logging.getLogger(__name__)


class product_template(models.Model):
    _inherit = "product.template"

    def on_change_loc_rack(self, cr, uid, ids, string, context=None):
        if context is None:
            context = {}

        if string:
            string = string.upper()
        return {'value': {'loc_rack': string}}

    ref_old = fields.Char(
        string="Ref old",
    )


class res_partner(models.Model):
    _inherit = "res.partner"

    quali_night_code = fields.Char(
        string="Quali night code",
    )
    partner_number_old = fields.Char(
        string="Partner number old",
    )


class product_supplierinfo(models.Model):
    _inherit = "product.supplierinfo"

    price_unit = fields.Float(
        string="Price",
        compute="_compute_prices"
    )
    price_landing = fields.Float(
        string="Landing Price",
        compute="_compute_prices"
    )

    price_unit_fc = fields.Float(
        string="Price foreign currency",
        compute="_compute_prices"
    )
    currency_fc = fields.Many2one(
        comodel_name="res.currency",
        string="Currency",
        compute="_compute_prices"
    )
    last_enquiry = fields.Date(
        string="Last enquiry",
    )
    supplier_text = fields.Char(
        string="Supplier text",
    )

    @api.multi
    def _compute_prices(self):
        cond_type_obj = self.env['product.trade.calculation.condition_type']
        landed_type = cond_type_obj.get_condition_type_for_landed()

        for record in self:
            purchase = record.product_trade_calculation_ids.filtered(
                lambda f:
                f.condition_type_id.use_purchase and f.state == 'enabled'
            )

            for condition in purchase:
                record.price_unit = condition.amount
                record.price_unit_fc = condition.amount_fc
                record.currency_fc = condition.amount_fc_currency
                break

            condition = landed_type.calculate_price(
                product=record.product_tmpl_id, supplier=record.name)
            if condition:
                record.price_landing = condition.amount
