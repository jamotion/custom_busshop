from openerp.osv import osv

class stock_invoice_on_shipping(osv.osv):

    _inherit = "stock.invoice.onshipping"

    def create_invoice(self, cr, uid, ids, context=None):

        res = super(stock_invoice_on_shipping, self).create_invoice(cr, uid, ids, context=context)

        stock = self.pool.get('stock.picking').browse(cr, uid, context['active_ids'])[0]

        # write client order ref of the order to the name of the associated invoice
        for inv in self.pool.get('account.invoice').browse(cr, uid, res):
            name = stock.sale_id.client_order_ref
            self.pool.get('account.invoice').write(cr, uid, [inv.id], {'name': name})

        return res

stock_invoice_on_shipping()