from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class sale_order(osv.Model):
    _inherit = 'sale.order'

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        res = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=context)

        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cr, uid, part)

        # Searches the partner invoice address first on the same level and otherwise set the company as invoice address
        if partner.parent_id.id:
            partner_id = partner_obj.search(cr, uid, [('type', '=', 'invoice'), ('parent_id', '=', partner.parent_id.id)], limit=1)
        else:
            partner_id = partner_obj.search(cr, uid, [('type', '=', 'invoice'), ('parent_id', '=', partner.id)], limit=1)

        if partner_id:
            res['value']['partner_invoice_id'] = partner_id[0]
        else:
            res['value']['partner_invoice_id'] = partner.parent_id.id or part

        return res

    def action_button_request(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.signal_workflow(cr, uid, ids, 'request')
        return True

    # gets the quali night code based on the delivery address
    def _get_quali_night_code(self, cr, uid, ids, arg, fields, context=None):
        res = {}

        for order in self.browse(cr, uid, ids):
            res[order.id] = order.partner_shipping_id.quali_night_code

        return res

    _columns = {
        'quali_night_code': fields.function(_get_quali_night_code, "Quali night code", type="char"),
        'date_order': fields.datetime('Date', required=True, readonly=True, select=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]}, copy=False),
        'user_id': fields.many2one('res.users', 'Salesperson', states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]}, select=True, track_visibility='onchange'),
        'partner_id': fields.many2one('res.partner', 'Customer', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]}, required=True, change_default=True, select=True, track_visibility='always'),
        'partner_invoice_id': fields.many2one('res.partner', 'Invoice Address', readonly=True, required=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]}, help="Invoice address for current sales order."),
        'partner_shipping_id': fields.many2one('res.partner', 'Delivery Address', readonly=True, required=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]}, help="Delivery address for current sales order."),
        'order_policy': fields.selection([
                ('manual', 'On Demand'),
                ('picking', 'On Delivery Order'),
                ('prepaid', 'Before Delivery'),
            ], 'Create Invoice', required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]},
            help="""On demand: A draft invoice can be created from the sales order when needed. \nOn delivery order: A draft invoice can be created from the delivery order when the products have been delivered. \nBefore delivery: A draft invoice is created from the sales order and must be paid before the products can be delivered."""),
        'pricelist_id': fields.many2one('product.pricelist', 'Pricelist', required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]}, help="Pricelist for current sales order."),
        'project_id': fields.many2one('account.analytic.account', 'Contract / Analytic', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'request': [('readonly', False)]}, help="The analytic account related to a sales order."),
        'order_line': fields.one2many('sale.order.line',
                                      'order_id',
                                      'Order Lines',
                                      readonly=False,
                                      states={'progress': [('readonly', True)],
                                              'done': [('readonly', True)]},
                                      copy=True),
        'picking_policy': fields.selection(
                [('direct', 'Deliver each product when available'),
                 ('one', 'Deliver all products at once')],
                'Shipping Policy', required=True, readonly=True,
                states={'draft': [('readonly', False)],
                        'sent': [('readonly', False)],
                        'request': [('readonly', False)]},
                help="""Pick 'Deliver each product when available' if you allow
                        partial delivery."""),
        'state': fields.selection([
            ('draft', 'Draft Quotation'),
            ('request', 'Quotation Request'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ], 'Status', readonly=True, track_visibility='onchange',
            help="Gives the status of the quotation or sales order. \nThe exception status is automatically set when a cancel operation occurs in the processing of a document linked to the sales order. \nThe 'Waiting Schedule' status is set when the invoice is confirmed but waiting for the scheduler to run on the order date.", select=True),
    }

sale_order()


class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    _description = 'Sales Order Line'
    _columns = {
        'price_unit': fields.float('Unit Price',
                                   required=True,
                                   digits_compute=dp.get_precision(
                                           'Product Price'),
                                   readonly=False,
                                   states={
                                       'progress': [('readonly', True)],
                                       'done': [('readonly', True)]
                                       }),
    }
