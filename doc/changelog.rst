Changelog
=========

Version 8.0.1.2.0
-----------------
    - Show alternative products and accessories in information tab of
      product.product and product.template form view
    - Add index.html for module documentation

Version 8.0.1.1.0
-----------------
Feature 1136:
    - Make unit prices of sale orders editable in all states except 'progress'
      & 'done'

Version 8.0.1.0.0
-----------------
Initial Release