{
    "name": "Custom Busshop",
    "version": "8.0.1.2.0",
    "category": "Sales",
    "author": "Jamotion GmbH",
    "summary": "Customer related changes",
    "depends": [
        "sale",
        "account",
        "product",
        "sale_stock",
        "purchase",
        "stock",
        "delivery",
        "fleet",
        "product_trade_calculation",
    ],
    "init_xml": [],
    "demo": [
        "data/pricelist_demo.xml"
    ],
    "data": [
        "views/sale.xml",
        "views/account.xml",
        "views/view.xml",
        "views/data.xml",
        "views/stock.xml",
        "views/warehouse.xml",
        "views/purchase_order.xml",
        "views/product.xml",
    ],
    "installable": True,
    "active": True
}
